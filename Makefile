md := $(wildcard ./%.md)
pdf := $(patsubst %.md,%.pdf,$(md))

all: $(pdf)

%.pdf: %.md
	pandoc --pdf-engine=xelatex -o $@ $<
